import { Body, Controller, Get, Param, Post, Query, Req } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getDefault(): string {
    return '<html><body><h1>Hello Buu</h1></body><html>';
  }
  @Get('Hello')
  getHello(): string {
    return '<html><body><h1>Hello</h1></body><html>';
  }
  @Post('World')
  getWorld(): string {
    return '<html><body><h1>World</h1></body><html>';
  }
  @Get('test-query')
  testQuery(
    @Req() req,
    @Query('celsius') celsius: number,
    @Query('type') type: string,
  ) {
    return {
      celsius: celsius,
      type: type,
    };
  }
  @Get('test-param/:celsius')
  testParam(@Req() req, @Param('celsius') celsius: number) {
    return {
      celsius,
    };
  }
  @Post('test-body')
  testBody(@Req() req, @Body() body, @Body('celsius') celsius: number) {
    return { celsius };
  }
}
